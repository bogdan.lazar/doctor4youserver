const mongoose = require("mongoose");

const DoctorSchema = new mongoose.Schema(
    {
        username: { type: String, required: true },
        email: { type: String, required: true, unique: true },
        password: { type: String, required: true },
        role: { type:Number, required:true },
        doctorFirstDescription: {type:String},
        doctorSecondDescription: {type:String},
        age: {type:String},
        img: {type:String},
        uid: { type: String, require: true, unique: true},
        imgName:{type:String},
        rank: {type:String},
        phone: {type:String}
    }
)

module.exports = mongoose.model("Doctor", DoctorSchema);