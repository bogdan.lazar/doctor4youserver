const mongoose = require("mongoose");

const ReviewSchema = new mongoose.Schema(
    {
        medicineName : {type: String, required: true},
        reviewFrom: {type: String, required: true},
        message: {type: String, required: true}
    }
)

module.exports = mongoose.model("Reviews", ReviewSchema);