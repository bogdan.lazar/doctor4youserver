const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
    {
        username: { type: String, required: true },
        email: { type: String, required: true, unique: true },
        password: { type: String, required: true },
        role: { type:Number, required:true },
        age: { type:Number },
        subscription: [
            {
                email:{
                    type:String
                }
            }
        ],
        img: {type:String},
        height:{type:String},
        width:{type:String},
        update:{type:String},
        pulse:{type:String},
        phone:{type:String},
        uid: { type: String, require: true, unique: true},
        imgName:{type:String},
        rank: {type:String}
    }
)

module.exports = mongoose.model("User", UserSchema);