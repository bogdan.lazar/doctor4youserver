const mongoose = require("mongoose");

const MedSchema = new mongoose.Schema(
    {
        name: { type:String, required:true, unique:true },
        price: { type:Number, required:true },
        description: { type:String, required:true },
        img: {type:String, required:true}
    }
)

module.exports = mongoose.model("Meds", MedSchema);