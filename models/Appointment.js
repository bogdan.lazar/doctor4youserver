const mongoose = require("mongoose");

const AppointmentSchema = new mongoose.Schema(
    {
        doctoremail: {type: String, required: true},
        day: {type:String, required:true},
        hour: {type:String, required:true},
        month: {type:String, required:true},
        year: {type:String, required:true},
        clients: {type:String, required:true},
        name: {type:String, required:true},
        surname: {type:String, required:true}
    }
)

module.exports = mongoose.model("Appointment", AppointmentSchema);