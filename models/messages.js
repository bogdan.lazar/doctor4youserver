const mongoose = require("mongoose");

const MessageSchema = new mongoose.Schema(
    {
        doctorusername: { type:String, required:true },
        clientusername: { type:String, required:true },
        doctor: { type:String, required:true },
        client: { type:String, required:true},
        roomid: { type:String, required: true, unique: true },
        Messagges: [
            {
                from:{ type:String, required:true},
                to:{ type:String, required:true},
                msg:{ type:String, required:true},
            }
        ],
        clientImg: { type:String, required:true },
        doctorImg: { type:String, required:true }
    }
)

module.exports = mongoose.model("Message", MessageSchema);