const mongoose = require("mongoose");

const CalculatorSchema = new mongoose.Schema(
    {
        Country: {type: String, required: true},
        City: {type: String, required: true},
        // trebuie sa fie de 2 feluri categoriile
        // sa aiba id: true daca exista acel tip de consult in timisoara de exemplu
        // sa aiba pretul pe cabinet pt consult
        Cardiology: [
            {
                type:{
                    type:String
                },
                place:{
                    type:String
                },
                price:{
                    type:String
                }
            }
        ],
        General_Surgery: [
            {
                type:{
                    type:String
                },
                place:{
                    type:String
                },
                price:{
                    type:String
                }
            }
        ],
        Internal_Medicine: [
            {
                type:{
                    type:String
                },
                place:{
                    type:String
                },
                price:{
                    type:String
                }
            }
        ],
        rate: {type:String, required: true}
    }
)

module.exports = mongoose.model("Calculator", CalculatorSchema);