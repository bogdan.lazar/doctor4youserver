const Doctor = require("../models/Doctors");
const { default : axios } = require("axios")

const handleAddDoctor = async(req,res) =>{
    const data = {
        uid: new Date().getTime(),
        name: "Dr. " + req.headers.username,
    }  
    console.log(req.headers);
    console.log("imgName");
    console.log(req.headers.username);
    console.log(req.headers.img);
    console.log(req.headers.doctorfirstdescription);
    console.log(req.headers.doctorseconddescription);
    const newDoctor = new Doctor({
        username: req.headers.username,
        email: req.headers.email,
        password: req.headers.password,
        role: 2011,
        doctorFirstDescription: req.headers.doctorfirstdescription,
        doctorSecondDescription: req.headers.doctorseconddescription,
        img: req.headers.img,
        uid:data.uid,
        imgName:req.headers.imgname,
        rank:req.headers.rank,
        phone:req.headers.phone
    });
    try{
        const savedDoctor = await newDoctor.save();
        if(savedDoctor)
        {
            console.log('aici');
            //TO BE UN COMM WHEN ACTIVATE NEW COMETCHAT
            axios.post(`https://api-eu.cometchat.io/v3.0/users`, JSON.stringify(data),{
                headers: {
                    'content-type': 'application/json',
                    'accept': 'application/json',
                    'apikey': '68057cff41ad81b416052a2b95f0f4718a284748',
                    'appid': '23937075945f69e5'
                } 
            }).then(res=>{
                console.log('success on adding user on cometchat');
            })
            .catch(err)
            {
                console.log("problem on cometChat");
            }
        }else{
            res
                .status(500)
                .json({
                    message: " Problem on cometChat"
                })
            console.log("problem on cometChat");
        }
        res.status(201).json(savedDoctor);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleDeleteDoctor = async(req,res) =>{
    try{
        await Doctor.findByIdAndDelete(req.headers._id);
        console.log("doctor deleted");
        res.status(201).json("success");
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetDoctorUnderId = async(req,res) =>{
    console.log(req.headers._id);
    try{
        const users = await Doctor.find({
            _id: { $all: [req.headers._id]}
        });
        res.status(201).json(users);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetDoctorUnderEmail = async(req,res) =>{
    try{
        const users = await Doctor.find({
            email: { $all: [req.headers.email]}
        });

        res.status(201).json(users);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleEditUser = async(req,res) => {
    console.log(req.headers._id);
    try{
        const doctor = await Doctor.findOneAndUpdate(
            req.headers._id,
            {
                "username": req.headers.username,
                "email": req.headers.email,
                "doctorFirstDescription": req.headers.doctorFirstDescription,
                "doctorSecondDescription": req.headers.doctorSecondDescription,
                "age":req.headers.age,
                "img":req.headers.img
            }
        );
        res.status(201).json(doctor);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetAllDoctor = async(req,res) => {
    console.log("getAllDoctors");
    try{
        const allDoctors = await Doctor.find();
        console.log(allDoctors);
        res.status(201).json(allDoctors);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetDoctorUserId = async(req,res) =>{
    try{
        const doctor = await Doctor.find({
            _id: { $all: [req.headers._id]}
        });
        res.status(201).json(doctor);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleEditDoctorFromPhone = async(req,res) =>{
    try{
        const doctor = await Doctor.findOneAndUpdate(
            req.body._id,
            {
                "username": req.body.username,
                "email": req.body.email,
                "age":req.body.age,
                "img":req.body.image
            }
        );
        res.status(201).json(doctor);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetDoctorUid = async(req,res) =>{
    try{
        console.log(req.headers.email);
        console.log("test");
        const doctor = await Doctor.find({
            email: { $all: [req.headers.email]}
        });
        res.status(201).json(doctor[0].uid);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetUsername = async(req,res) =>{
    try{
        const users = await Doctor.find({
            email: { $all: [req.headers.email]}
        });
        //console.log(users);
        res.status(201).json(users[0].username);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetImg = async(req,res) =>{
    try{
        const users = await Doctor.find({
            email: { $all: [req.headers.email]}
        });
        console.log(users);
        res.status(201).json(users[0].imgName);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetDoctorEmailUnderId = async (req,res) => {
    console.log("Handle Doctor email under Id");
    console.log(req.headers._id)
    try{
        const doctor = await Doctor.find({
            _id : { $all: [req.headers._id]}
        });
        console.log(doctor);
        res.status(201).json(doctor[0].email);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleCheckDoctorAlreadyExist = async(req,res) => {
    console.log(req.headers.email);
    try{
        const doctor = await Doctor.find({
            email: { $all: [req.headers.email]}
        });
        if(doctor)
        {
            console.log('sedind back positive results')
            res.status(201).json("user already exists");
        }else{
            console.log('sedind back negative results')
            res.status(201).json("user not exists");
        }
    }catch(err)
    {
        res.status(500).json(err);
    }
}


module.exports = {handleGetDoctorUnderEmail, handleCheckDoctorAlreadyExist, handleGetImg, handleEditDoctorFromPhone, handleAddDoctor, handleDeleteDoctor, handleGetDoctorUnderId, handleEditUser, handleGetAllDoctor,handleGetDoctorUserId, handleGetDoctorUid, handleGetUsername, handleGetDoctorEmailUnderId};