const { json } = require("express");
const Calculator = require("../models/Calculator");
const { default : axios } = require("axios");

const handleAddCalculator = async(req,res) => {
    console.log(req.headers);
    console.log("add cabinet");
    const newCalculator = new Calculator(
        {
            Country: req.headers.country,
            City: req.headers.city,
            Cardiology: [
                {
                    type:req.headers.cardiologytype,
                    place:req.headers.cardiologyplace,
                    price:req.headers.cardiologyprice
                }
            ],
            General_Surgery: [
                {
                    type:req.headers.generalsurgerytype,
                    place:req.headers.generalsurgeryplace,
                    price:req.headers.generalsurgeryprice
                }
            ],
            Internal_Medicine: [
                {
                    type:req.headers.internalmedicinetype,
                    place:req.headers.internalmedicineplace,
                    price:req.headers.internalmedicineprice
                }
            ],
            rate: req.headers.rate
        }
    )

    try{
        const savedCalculator = await newCalculator.save();
        console.log("it's ok");
        console.log(savedCalculator);
        res.status(201).json(savedCalculator);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleDeleteCalculator = async(req,res) => {

    try{
        const res = await Calculator.findByIdAndDelete(req.headers._id);
        console.log(res);
        res.status(201).json("index is deleted correctly");
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetCalculatorUnderCountryCityCardiology = async(req,res) => {
    try{
        // de regandi
        console.log("get calculator");
        console.log(req.headers);
        console.log(req.headers.cardiologytype);

        const response = await Calculator.find({
            Country: { $all: [req.headers.country] },
            City: { $all: [req.headers.city] },
            Cardiology:{$elemMatch:{type: req.headers.cardiologytype}}  
        });
        
        console.log("after response output");
        //console.log(response[0].Cardiology[0]);
        console.log(response.length);
        if(response.length == 0)
        {
            res.status(201).json("nothing from database");
        }else{
            res.status(201).json(response[0].Cardiology);
        }
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetCalculatorUnderCountryCityGeneralSurgery = async(req,res) => {
    try{
        // de regandi
        console.log("get calculator");
        console.log(req.headers);

        const response = await Calculator.find({
            Country: { $all: [req.headers.country] },
            City: { $all: [req.headers.city] },
            General_Surgery:{$elemMatch:{type: req.headers.generalsurgerytype}}  
        });
        
        console.log("after response output");
        //console.log(response[0].Cardiology[0]);
        console.log(response.length);
        if(response.length == 0)
        {
            res.status(201).json("nothing from database");
        }else{
            res.status(201).json(response[0].Cardiology);
        }
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetCalculatorUnderCountryCityInternalMedicine = async(req,res) => {
    try{
        // de regandi
        // console.log("get calculator");
        console.log(req.headers);

        const response = await Calculator.find({
            Country: { $all: [req.headers.country] },
            City: { $all: [req.headers.city] },
            Internal_Medicine:{$elemMatch:{type: req.headers.internalmedicinetype}}  
        });
        
        console.log("after response output");
        //console.log(response[0].Cardiology[0]);
        console.log(response.length);
        if(response.length == 0)
        {
            res.status(201).json("nothing from database");
        }else{
            res.status(201).json(response[0].Cardiology);
        }
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetAll = async(req,res) => {
    try{
        const allCalculator = await Calculator.find();
        res.status(201).json(allCalculator);
    }catch(err){
        res.status(500).json(err);
    }
}

module.exports = {handleGetAll, handleGetCalculatorUnderCountryCityInternalMedicine, handleGetCalculatorUnderCountryCityGeneralSurgery,handleGetCalculatorUnderCountryCityCardiology, handleDeleteCalculator, handleAddCalculator};