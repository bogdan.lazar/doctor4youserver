const App = require("../models/Appointment");
const User = require("../models/User");

const handleAddApp = async(req,res) => {
    console.log("The beggining of the function handleApp");
    console.log(req.headers.name);
    console.log(req.headers.surname);
    const newApp = new App({
        doctoremail:req.headers.doctoremail,
        day:req.headers.day,
        hour:req.headers.hour,
        clients:req.headers.clients,
        month:req.headers.month,
        year:req.headers.year,
        name:req.headers.name,
        surname:req.headers.surname
    })
    console.log("The new APP is saved");
    console.log(newApp);

    try{
        const savedApp = await newApp.save();
        res.status(201).json(savedApp);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleDeleteApp = async(req,res) => {
    try{
        const AppDeleted = await App.findByIdAndDelete(req.headers._id);
        console.log("deleted appointment");
        res.status(201).json("deleted");
    }catch(err){
        res.status(500).json(err);
    }
}

const handleEditApp = async(req,res) =>{
    try{
        const app = await App.findOneAndUpdate(
            req.body._id,
            {
                "doctoremail":req.body.doctoremail,
                "day":req.body.day,
                "hour":req.body.hour,
                "clients":req.body.clients,
                "month":req.body.month,
                "year":req.body.year,
                "name":req.body.name,
                "surname":req.body.surname
            }
        )
        res.status(201).json("App updated");
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetAllApp = async(req,res) =>{
    try{
        const AllApps = await App.find();
        res.status(201).json(AllApps);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetSomeClientsUnderEmail = async(req,res) => {

    try{
        const getDoctor = await App.find({
            doctoremail : { $all: [req.headers.doctoremail]},
            year : { $all: [req.headers.year]},
            month : { $all: [req.headers.month]},
            day : { $all: [req.headers.day]}
        });
        res.status(201).json(getDoctor);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetDoctorApps = async(req,res) => {
    try{
        const getDoctorApp = await App.find(
            {
                doctoremail : { $all: [req.headers.doctoremail]},
                year : { $all: [req.headers.year]},
                month : { $all: [req.headers.month]},
                day : { $all: [req.headers.day]}
            }
        );
        res.status(201).json(getDoctorApp);
    }catch(err){
        res.status(500).json(err);
    }
}

module.exports = { handleAddApp, handleDeleteApp, handleEditApp, handleGetAllApp, handleGetSomeClientsUnderEmail, handleGetDoctorApps }