const Review = require("../models/reviews");

const handleAddReview = async(req,res) => {
    const newReview = new Review({
        medicineName : req.body.medicineName,
        reviewFrom: req.body.reviewFrom,
        message: req.body.message
    });

    try{
        const savedRew = await newReview.save();
        res.status(201).json("added successfully");
    }catch(err){
        res.status(500).json(err);
    }
}

const handleDeleteReview = async(req,res) => {
    try{
        await Review.findByIdAndDelete(req.body._id);
        res.status(201).json("review deleted");
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetReviewUnderCondition = async(req,res) => {
    try{
        const users = await Review.find({
            medicineName: { $all: ["test2"]}
        });
        res.status(201).json(users);
    }catch(err){
        res.status(500).json(err);
    }
}

module.exports = {handleAddReview, handleDeleteReview, handleGetReviewUnderCondition}