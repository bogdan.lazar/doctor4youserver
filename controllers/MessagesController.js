const Message = require("../models/messages");

const handleAddNewChat = async (req,res) => {
    console.log(req.headers.client);
    console.log(req.headers.doctor);
    console.log(req.headers.roomid);
    console.log(req.headers.doctorusername);
    console.log(req.headers.clientusername);
    console.log(""+ req.headers.clientimg);
    console.log(req.headers.doctorimg);
    let finalRoomId="" + req.headers.roomid;
    console.log('finalroomid');
    console.log(finalRoomId);
    const newMessage = new Message({
        doctorusername: req.headers.doctorusername,
        clientusername: req.headers.clientusername,
        doctor: req.headers.doctor,
        client: req.headers.client,
        roomid: finalRoomId,
        Message:[
            {
                from:"",
                to:"",
                msg:""
            }
        ],
        clientImg: req.headers.clientimg,
        doctorImg: req.headers.doctorimg
    });
    console.log(finalRoomId);
    try{
        const saveMess = await newMessage.save();
        console.log(saveMess);
        res.status(201).json(saveMess);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleDeleteMessage = async (req,res) => {
    try{
        await Message.findByIdAndDelete(req.headers._id);
        res.status(201).json("message deleted");
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetMessageForClient = async (req,res) => {
    console.log(req.headers.client);
    try{
        const messages = await Message.find({
            client: { $all: [req.headers.client]}
        });
        //console.log(messages);
        res.status(201).json(messages);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetMessageForDoctor = async (req,res) => {
    console.log(req.headers.doctor);
    try{
        const messages = await Message.find({
            doctor: { $all: [req.headers.doctor]}
        });
        //console.log(messages);
        res.status(201).json(messages);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleAddMessageConversation = async(req,res) => {
    console.log(req.headers.to);
    console.log(req.headers.from);
    console.log(req.headers.roomid);
    console.log(req.headers.msg);
    try{
        const result = await Message.updateOne(
            {
                roomid: req.headers.roomid
            },
            {
                $push: {Messagges :{ from: req.headers.from, to: req.headers.to, msg: req.headers.msg, clientImg: req.headers.clientImg, doctorImg: req.headers.doctorImg}}
            }
        );
        res.status(201).json("message added succesfully");
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetMessageConversationRoomId = async(req,res) => {
    console.log(req.headers.roomid);
    console.log("handleMessageGetConversation");
    try{
        const messages = await Message.find({
            roomid: {$all: [req.headers.roomid] }
        });
        //console.log(messages[0].Messagges);
        res.status(201).json(messages[0]);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetMessageConversationRoomIdDoctor = async(req,res) => {
    console.log(req.headers.roomid);
    console.log("handleMessageGetConversation");
    try{
        const messages = await Message.find({
            roomid: {$all: [req.headers.roomid] }
        });
        console.log("sending");
        console.log(messages[0]);
        res.status(201).json(messages[0]);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetClientImg = async(req, res) => {
    console.log(req.headers.clientEmail);
    try{
        const messages = await Message.find({
            client: {$all: [req.headers.clientEmail]}
        });
        console.log(messages.clientImg);
        res.status(201).json(messages[0].clientImg);
    }catch(err)
    {
        res.status(500).json(err);
    }
}

const handleGetDoctorImg = async(req, res) => {
    console.log(req.headers.doctorEmail);
    try{
        const messages = await Message.find({
            doctor: {$all: [req.headers.doctorEmail]}
        });
        console.log(messages.doctorEmail);
        res.status(201).json(messages[0].doctorEmail);
    }catch(err)
    {
        res.status(500).json(err);
    }
}

module.exports = {handleGetClientImg, handleGetDoctorImg, handleAddNewChat, handleDeleteMessage, handleGetMessageForClient, handleGetMessageForDoctor, handleAddMessageConversation, handleGetMessageConversationRoomId, handleGetMessageConversationRoomIdDoctor};