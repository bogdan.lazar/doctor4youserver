const Form = require("../models/Form");

const handleAddForm = async(req,res) => {
    const newForm = new Form({
        name:req.headers.name,
        email:req.headers.email,
        message:req.headers.message
    });
    try{
        const savedForm = await newForm.save();
        res.status(201).json(savedForm);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleDeleteForm = async(req,res) => {
    try{
        const test = await Form.findByIdAndDelete(req.headers._id);
        res.status(201).json("form deleted");
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetAllForms = async(req,res) => {
    try{
        const Forms = await Form.find();
        res.status(201).json(Forms);
    }catch(err){
        res.status(500).json(err);
    }
}

module.exports = { handleAddForm, handleDeleteForm, handleGetAllForms }