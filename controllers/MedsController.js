const Med = require("../models/meds");

const handleNewMeds = async (req,res) => {
    // here to create a new meds

    const newMed = new Med({
        name: req.headers.name,
        price: req.headers.price,
        description: req.headers.description,
        img: req.headers.img
    });
    try{
        const savedMeds = await newMed.save();
        res.status(201).json(savedMeds);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleDeleteMeds = async(req,res) => {
    console.log(req.headers._id);   
    try{
        await Med.findByIdAndDelete(req.headers._id);
        res.status(201).json("meds deleted");
    }catch(err){
        res.status(500).json(err);
    }
}

const haldeEditMeds = async(req,res) => {
    console.log(req.headers._id);
    try{
        const meds = await Med.findByIdAndUpdate(
            req.headers._id,
            {
                "name": req.headers.name,
                "price": req.headers.price,
                "description":req.headers.description,
                "img":req.headers.img
            }
        );
        console.log("this med");
        console.log(meds);
        res.status(201).json(meds);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetAllMeds = async(req,res) => {
    try{
        const meds = await Med.find();
        res.status(201).json(meds);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetMedsUnderCondition = async(req,res) => {
    try{
        const meds = await Med.findById(req.headers._id);
        res.status(201).json(meds);
    }catch(err){
        res.status(500).json(err);
    }
}

module.exports = { handleNewMeds, handleDeleteMeds, haldeEditMeds, handleGetAllMeds, handleGetMedsUnderCondition };