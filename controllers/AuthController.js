const User = require("../models/User");
const Doctor = require("../models/Doctors");
const { default : axios } = require("axios")

const handleRegisterUser = async (req, res) => {
    // here to create a new user
    console.log(req.headers.rank);
    const data = {
        uid: new Date().getTime(),
        name: req.headers.username,
    }
    const time = new Date();
    const newUser = new User({
        username: req.headers.username,
        email: req.headers.email,
        password: req.headers.password,
        role: 1011,
        age: req.headers.age,
        subscription: req.headers.subscription,
        img:req.headers.img,
        uid:data.uid,
        height:"",
        width:"",
        pulse:"",
        update:time,
        phone:req.headers.phone,
        rank:req.headers.rank
    });

    
    try {
        const savedUser = await newUser.save();
        if(savedUser){
            axios.post(`https://api-eu.cometchat.io/v3.0/users`, JSON.stringify(data),{
                headers: {
                    'content-type': 'application/json',
                    'accept': 'application/json',
                    'apikey': '68057cff41ad81b416052a2b95f0f4718a284748',
                    'appid': '23937075945f69e5'
                } 
            }).then(res=>{
                console.log('success on adding user on cometchat');
            }).catch(err=>{
                console.log(err);
            })
        }else{
            res
                .status(500)
                .json({
                    message: " Problem on cometChat"
                })
        }
        // const options = {
        //     method: 'POST',
        //     headers: {Accept: 'application/json', 'Content-Type': 'application/json'},
        //     body: JSON.stringify({accepted: ['testesd']})
        //   };
        // fetch('https://218089f1f04422d7.api-eu.cometchat.io/v3/users/user/120228123437/friends', options)
        //   .then(response => response.json())
        //   .then(response => console.log(response))
        //   .catch(err => console.error(err));
        res.status(201).json(savedUser);
    } catch (err) {
        res.status(500).json(err);
    }
}

// 300 pt wrong credential


const haldeLoginUser = async(req,res) => {
    console.log("LOGIN FUNCTION");
    console.log(req.headers.email);
    const user = await User.findOne({email:req.headers.email});
    if(!user){
        res.status(300).json("wrong email");
        return;
    }
    if(req.headers.password != user.password){
        res.status(300).json("wrong password");
        return;
    }

    try{
        res.status(201).json(user);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleLoginFacebookGoogle = async (req,res) => {
    console.log(req.headers.email);
    const user = await User.findOne({email:req.headers.email});
    try{
        res.status(201).json(user);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleDoctorLogin = async(req,res) => {
    console.log("doctor handleDoctorLogin");
    console.log(req.headers.email);
    const doctor = await Doctor.findOne({email:req.headers.email});
    if(!doctor){
        res.status(300).json("wrong email");
        return;
    }
    console.log("pass");
    console.log(doctor.password);
    if(req.headers.password != doctor.password){
        res.status(300).json("wrong password");
        return;
    }
    try{
        res.status(201).json(doctor);
    }catch(err){
        res.status(500).json(err);
    }
}

module.exports = { handleRegisterUser, haldeLoginUser, handleDoctorLogin,handleLoginFacebookGoogle};