const { json } = require("express");
const User = require("../models/User");
const { default : axios } = require("axios");
const { update } = require("../models/User");

const handleDeleteUser = async(req,res) => {
    console.log("1 test");
    console.log(req.headers._id);
    try{
        const res = await User.findByIdAndDelete(req.headers._id);
        console.log("deleted user");
        console.log("res");
        console.log(res);
        res.status(201).json("user is deleted correctly");
    }catch(err){
        res.status(500).json(err);
    }
}

// aici se poate baga conditia pt doctori sa se vada clienti
// si invers pentru clienti sa vada doctorul

const handleGetUserUnderEmail = async(req,res) => {
    // console.log(req.headers.email);
    try{
        const Getusers = await User.find({
            email: { $all: [req.headers.email]}
        });
        res.status(201).json(Getusers);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetUserUnderEmailForChat = async(req,res) => {
    console.log(req.headers.email);
    try{
        const Getusers = await User.find({
            email: { $all: [req.headers.email]}
        });
        // console.log(Getusers);
        console.log(Getusers[0].subscription);
        res.status(201).json(Getusers);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetUserUnderDoctorEmail = async(req,res) =>{
    try{
        const users = await User.find({
            subscription:{$elemMatch:{email: req.headers.email}}  
        });
        res.status(201).json(users);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleEditUser = async(req,res) => {

    try{
        const user = await User.findOneAndUpdate(
            req.body._id,
            {
                "username": req.body.username,
                "email": req.body.email
            }
        );
        res.status(201).json(user);
    }catch(err){
        res.status(500).json(err);
    }
}

// to be deleted
// const handleAddDoctor = async (req, res) => {
//     // here to create a new user
//     const newUser = new User({
//         username: req.body.username,
//         email: req.body.email,
//         password: req.body.password,
//         role: req.body.role,
//         doctorFirstDescription: req.body.doctorFirstDescription,
//         doctorSecondDescription: req.body.doctorSecondDescription
//     });
//     try {
//         const savedUser = await newUser.save();
//         res.status(201).json(savedUser);
//     } catch (err) {
//         res.status(500).json(err);
//     }
// }

const handleAddClient = async(req,res) => {
    // console.log("test");
    // console.log(req.body);
    const data = {
        uid: new Date().getTime(),
        name: req.headers.username,
    }
    console.log("rankkkkk");
    console.log(req.headers.rank);

    const date = new Date().getTime();
    console.log(req.headers.imgname);
    const newUser = new User({
        username: req.headers.username,
        email: req.headers.email,
        password: req.headers.password,
        role: 1011,
        age: req.headers.age,
        subscription: req.headers.subscription,
        img:req.headers.img,
        uid:data.uid,
        height:"",
        width:"",
        pulse:"",
        update:date,
        phone:req.headers.phone,
        imgName:req.headers.imgname,
        rank:req.headers.rank
    });
    // console.log(data);
    try {
        const savedUser = await newUser.save();
        if(savedUser){
            axios.post(`https://api-eu.cometchat.io/v3.0/users`, JSON.stringify(data),{
                headers: {
                    'content-type': 'application/json',
                    'accept': 'application/json',
                    'apikey': '68057cff41ad81b416052a2b95f0f4718a284748',
                    'appid': '23937075945f69e5'
                } 
            }).then(res=>{
                console.log('success on adding user on cometchat');
            }).catch(err=>{
                //console.log(err);
                console.log('err commetchat');
            })
        }else{
            res
                .status(500)
                .json({
                    message: " Problem on cometChat"
                })
        }
        res.status(201).json(savedUser);
    } catch (err) {
        res.status(500).json(err);
    }
}

const handleGetAllUser = async(req,res) => {
    try{
        console.log("INAINTE DE ALLUSERS");
        const allUsers = await User.find();
        console.log("TEST DUPA ALLUSERS");
        res.status(201).json(allUsers);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetUserUnderId = async(req,res) =>{
    console.log(req.headers._id);
    try{
        const Getuser = await User.find({
            _id: { $all: [req.headers._id]}
        });
        res.status(201).json(Getuser);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleAddSub = async(req,res) =>{
    
    //console.log(req.headers);

    console.log("adding new sub at the beginning");
    console.log(req.headers.user);
    console.log(req.headers.doctor);
    console.log(req.headers.uiduser);
    console.log(req.headers.uiddoctor);

    try{
        const options = {
            method: 'POST',
            headers: {
              accept: 'application/json',
              'Content-Type': 'application/json',
              Accept: 'application/json',
              'apikey': '68057cff41ad81b416052a2b95f0f4718a284748',
              'appid': '23937075945f69e5'
            },
            body: JSON.stringify({accepted: [req.headers.uiddoctor]})
          };
          
          fetch('https://api-eu.cometchat.io/v3.0/users/'+ req.headers.uiduser + '/friends', options)
            .then(response => response.json())
            .then(response => console.log(response))
            .catch(err => console.error(err));
    }catch(err){

    }

    try{
        const result = await User.updateOne(
            {
                email:req.headers.user
            },
            {
                $push: {subscription :{ email: req.headers.doctor}}
            }
        );
        res.status(201).json(result);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleEditUserFromMobile = async(req,res) =>{
    console.log(req.body.username);
    console.log(req.body.email);
    console.log(req.body.age);
    console.log(req.body._id);
    const time = new Date();
    console.log(time);
    try{

        const user = await User.findOneAndUpdate(
            {_id: req.body._id},
            {
                "username": req.body.username,
                "email": req.body.email,
                "age":req.body.age,
                "img":req.body.image,
                "update": time
            }
        );
        res.status(201).json(user);

    }catch(err){
        res.status(500).json(err);
    }
}

const handleAddInfoFromMobile = async(req,res) => {
    try{
        const date = new Date().toLocaleString() + "";
        const user = await User.findOneAndUpdate(
            {_id: req.body._id},
            {
                "height": req.body.height,
                "width": req.body.width,
                "pulse":req.body.pulse,
                "update":date
            }
        );
        // const user = await User.find({
        //     _id: { $all: [req.body._id]}
        // });
        res.status(201).json(user);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleAddInfoFromHardware = async(req,res) => {

    console.log("this is a message got from hardware");
    console.log(req.params);
    console.log(req.params.email);
    console.log(req.params.id);
    try{
        const date = new Date().toLocaleString() + "";
        const user = await User.findOneAndUpdate(
            {email: req.params.email},
            {
                "pulse":req.params.id
            }
        );
        res.status(200).json("OK");
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetUserUid = async(req,res)=>{
    try{
        const Getuser = await User.find({
            email: { $all: [req.headers.email]}
        });
        console.log("returning user UID");
        console.log(Getuser[0].uid);
        console.log("Done returning user uid");
        res.status(201).json(Getuser[0].uid);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetUsername = async(req,res) => {
    try{
        const Getusers = await User.find({
            email: { $all: [req.headers.email]}
        });
        // console.log("handle get username");
        // console.log(Getusers[0].username);
        res.status(201).json(Getusers[0].username);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleGetImg= async(req,res) => {
    console.log("HandleGetImg");
    console.log(req.headers.email);
    try{
        const Getusers = await User.find({
            email: { $all: [req.headers.email]}
        });
        // console.log("handle get username");
        // console.log(Getusers[0].username);
        res.status(201).json(Getusers[0].imgName);
    }catch(err){
        res.status(500).json(err);
    }
}

const handleCheckUserAlreadyExist = async(req,res) => {
    console.log("printing email from checkUserALreaadyExists")
    console.log(req.headers.email);
    try{
        const user = await User.find({
            email: { $all: [req.headers.email]}
        });
        console.log(user);
        //console.log(user[0].email);
        if(user.length === 0)
        {
            res.status(201).json("user not exists");
        }else{
            res.status(201).json("user already exists");
        }
    }catch(err)
    {
        res.status(500).json("user already exists")
    }
}

module.exports = {handleAddInfoFromHardware, handleCheckUserAlreadyExist, handleGetImg, handleGetUserUid, handleAddSub, handleDeleteUser, handleGetUserUnderEmail, handleEditUser, handleGetAllUser, handleAddClient, handleGetUserUnderDoctorEmail,handleGetUserUnderId, handleEditUserFromMobile, handleAddInfoFromMobile, handleGetUserUnderEmailForChat, handleGetUsername};