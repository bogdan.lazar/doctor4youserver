const express = require('express');
const router = express.Router();

const AppController = require("../controllers/AppointmentController");

router.post("/addApp", AppController.handleAddApp);

router.delete("/deleteApp", AppController.handleDeleteApp);

router.put("/editApp", AppController.handleEditApp);

router.get("/getAllApp", AppController.handleGetAllApp);

router.get("/getAppUnderEmail", AppController.handleGetSomeClientsUnderEmail);

router.get("/getDoctorApp", AppController.handleGetDoctorApps);

module.exports = router;