const express = require('express');
const router = express.Router();
const authController = require("../controllers/AuthController")

router.post('/register', authController.handleRegisterUser);

router.post("/login", authController.haldeLoginUser);

router.post("/doctorLogin", authController.handleDoctorLogin);

router.post("/loginFacebookGoogle", authController.handleLoginFacebookGoogle);

module.exports = router;