const express = require('express');
const router = express.Router();
const UserController = require("../controllers/UserController");

router.delete("/deleteUser", UserController.handleDeleteUser);

router.get("/getUserUnderEmail", UserController.handleGetUserUnderEmail);

router.get("/getUserUnderEmailForChat", UserController.handleGetUserUnderEmailForChat);

router.put("/updateUser", UserController.handleEditUser);

router.post("/addClient", UserController.handleAddClient)

router.get("/getAllUsers", UserController.handleGetAllUser);

router.get("/getUserUnderDoctorEmail", UserController.handleGetUserUnderDoctorEmail)

router.get("/getUserUnderId",UserController.handleGetUserUnderId);

router.post("/AddSub", UserController.handleAddSub);

router.put("/EditUserFromMobile", UserController.handleEditUserFromMobile);

router.put("/addInfoFromMobile", UserController.handleAddInfoFromMobile);

router.get("/getUserUid", UserController.handleGetUserUid);

router.post('/addInfoHardware/:email/:id', UserController.handleAddInfoFromHardware);

router.get('/testingHardware?:id', UserController.handleAddInfoFromHardware);

router.get('/getUsername', UserController.handleGetUsername);

router.get("/getClientImg", UserController.handleGetImg);

router.get("/checkUserAlreadyExists", UserController.handleCheckUserAlreadyExist);

module.exports = router;
