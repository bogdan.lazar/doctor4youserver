const express = require('express');
const router = express.Router();
const DoctorController = require("../controllers/DoctorsController");

router.post("/addDoctor", DoctorController.handleAddDoctor);

router.delete("/deleteDoctor", DoctorController.handleDeleteDoctor);

router.put("/editDoctor", DoctorController.handleEditUser);

router.put("/editDoctorPhone", DoctorController.handleEditDoctorFromPhone);

router.get("/getDoctorUnderId", DoctorController.handleGetDoctorUnderId);

router.get("/getAllDoctors", DoctorController.handleGetAllDoctor);

router.get("/getDoctor", DoctorController.handleGetDoctorUserId);

router.get("/getDoctorUnderEmail", DoctorController.handleGetDoctorUnderEmail);

router.get("/getDoctorUid", DoctorController.handleGetDoctorUid);

router.get("/getUsername", DoctorController.handleGetUsername);

router.get("/getDoctorImg", DoctorController.handleGetImg);

router.get("/getEmailUnderId", DoctorController.handleGetDoctorEmailUnderId);

router.get("/checkDoctorAlreadyExists", DoctorController.handleCheckDoctorAlreadyExist);

module.exports = router;