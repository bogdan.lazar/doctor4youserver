const express = require('express');
const router = express.Router();

const ReviewController = require("../controllers/ReviewController");

router.post("/addReview", ReviewController.handleAddReview);

router.get("/removeReview", ReviewController.handleDeleteReview);

router.get("/getReviewsUnderName", ReviewController.handleGetReviewUnderCondition);


module.exports = router;