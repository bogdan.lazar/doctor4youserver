const express = require('express');
const router = express.Router();

const FormController = require("../controllers/FormController");

router.post("/addForm", FormController.handleAddForm);

router.delete("/deleteForm", FormController.handleDeleteForm);

router.get("/allForms", FormController.handleGetAllForms);

module.exports = router;