const express = require('express');
const router = express.Router();
const CalculatorController = require("../controllers/CalculatorController")

router.delete("/deleteCalculator", CalculatorController.handleDeleteCalculator);

router.get("/getCalculatorUnderCountryCityCardiology", CalculatorController.handleGetCalculatorUnderCountryCityCardiology);

router.get("/getCalculatorUnderCountryCityGeneralSurgery", CalculatorController.handleGetCalculatorUnderCountryCityGeneralSurgery);

router.get("/getCalculatorUnderCountryCityInternalMedicine", CalculatorController.handleGetCalculatorUnderCountryCityInternalMedicine);

router.post("/addCalculator", CalculatorController.handleAddCalculator);

router.get("/allCalculators", CalculatorController.handleGetAll);

module.exports = router;