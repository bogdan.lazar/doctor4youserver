const express = require('express');
const router = express.Router();

const MessageController = require("../controllers/MessagesController");

router.post("/addMessage", MessageController.handleAddNewChat);

router.delete("/deleteMessage", MessageController.handleDeleteMessage);

router.get("/clientMessage", MessageController.handleGetMessageForClient);

router.get("/doctorMessage", MessageController.handleGetMessageForDoctor);

router.post("/addMessageConversation", MessageController.handleAddMessageConversation);

router.get("/getRoomMessage", MessageController.handleGetMessageConversationRoomId);

router.get("/getMessageDoctor", MessageController.handleGetMessageConversationRoomIdDoctor);

router.get("/getClientImg", MessageController.handleGetClientImg);

router.get("/getDoctorImg", MessageController.handleGetDoctorImg);

module.exports = router;