const express = require('express');
const router = express.Router();

const MedsController = require("../controllers/MedsController");

router.post("/addMeds", MedsController.handleNewMeds);

router.delete("/removeMeds", MedsController.handleDeleteMeds);

router.put("/editMeds", MedsController.haldeEditMeds);

router.get("/getAllMeds", MedsController.handleGetAllMeds);

router.get("/getMedsUnderCondition", MedsController.handleGetMedsUnderCondition)

module.exports = router;