const axios = require('axios');

//creating a room

const createRoom = async (payload) => {
    return await axios.post("http://localhost:2000/v1/room", payload).then(({data}) =>{
        return{
            data:data
        }
    }).catch(function (error) {
        if(error.response != undefined){
            return error.response.data;
        }else {
            return {
                error: "Uknow error"
            }
        }
    });
}

module.exports = {createRoom}