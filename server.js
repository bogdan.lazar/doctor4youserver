const express = require("express");
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');
const http = require('http');
var chat = require('./chat');
var socketio = require('socket.io');
const uuid = require('uuid')
const { createMessage, loadMessages } = require('./controllers/messageRoom');


dotenv.config();

mongoose
    .connect(process.env.MONGO_URL)
    .then(() => console.log("DB Connection Successful!"))
    .catch((err) => {
        console.log(err);
    });

var server = http.createServer(app);
const io = require('socket.io')(server);

io.on("connection", socket =>{
    console.log("A user connected :D");
    socket.on("chat message", msg => {
        console.log(msg);
        io.emit("chat message", msg);
    });

    socket.on("join", async ({id,username}, callback) =>{
        console.log(socket.id);
        const user = {
            id: id,
            username: username
        };
        console.log("test incercat de mine de join");
        console.log(id);
        console.log(username);
        
        // socket.join(id);
        // socket.emit("message", 
        //     // {
        //     // _id: uuid.v4(),
        //     // text: "${user.name}, welcome to this channel",
        //     // createdAt: new Date(),
        //     // system: true,
        //     // room_id: id
        //     // }
        //     "testDeTrimis"
        // );
        // io.to(id).emit('message', 
        //     // {
        //     //     _id: uuid.v4(),
        //     //     text: "${user.name}, welcome to this channel",
        //     //     createdAt: new Date(),
        //     //     system: true,
        //     //     room_id: id
        //     // }
        //     "trimisCatreTotiInafaraDeSender"
        // );

        //callback();
    });

    socket.on("message", ({id,message})=>{
        console.log("test incercat de mine de message");
        console.log(id);
        console.log(message);
        console.log(socket.id);
        socket.join(id);
        socket.to(id).to(socket.id).emit("message", message);
    })

    socket.on("loadmessages", async (data,callback) => {
        let room = data.id;
        console.log('room', room);
        // aici se cheama mesajele din mongodb, still ongoing
        const loadmessages = await loadMessages(room);

    });

    socket.on("send", async (data, callback) => {
        console.log(data, data.room)
        // salvare mesaj
        // const createmessage = await createMessage(data)

        // if (createmessage.error) return callback({
        //     status: false,
        //     message: createmessage.error
        // })

        // socket.emit('message', data);
        socket.broadcast.to(data.room).emit('message', data);
        callback({
            status: true,
            message: data
        })
    });
});


server.listen(3000, () => console.log("server runnning on port 3000"));

app.use(cors());
app.use(express.json());
app.use("/auth",require('./routes/AuthRoute'));
app.use("/meds", require('./routes/MedsRoute'));
app.use("/users", require('./routes/UserRoute'));
app.use("/reviews", require('./routes/ReviewRoute'));
app.use("/forms", require("./routes/FormRoute"));
app.use("/App", require("./routes/AppointmentRoute"));
app.use("/doctor", require("./routes/DoctorsRoute"));
app.use("/messages", require("./routes/MessagesRoute"))
app.use("/calculator", require("./routes/CalculatorRoute"))

app.listen(process.env.PORT || 2000, () => {
    console.log("Backend server is running!");
});